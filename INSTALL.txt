Requirements
------------

This module works on Drupal 5.x. You need also :
 - Google Earth client : http://earth.google.com/
 - GeoIP country (or city) database : http://www.maxmind.com/app/country/

Installation
------------

1. Extract the google earth module to the "modules" directory.

2. Enable the module in Administer >> Site building >> Modules.
   Set the permissions to your liking in Administer >> User management.

3. Establish the google earth settings in Administer >> Site configuration >> Google Earth map.

4. Generate a map using Administer >> Site configuration >> Google Earth map >> Generate Google Earth map.

5. Download the map in Navigation >> Google Earth map

Author
------

http://www.bonvga.net/contact
